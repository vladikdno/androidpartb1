package com.example.androidpartb1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    private EditText editText;

    private Button buttonClear;
    private Button returnButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final LinkedList<String> stringLinkedList = new LinkedList<>();

        buttonClear = findViewById(R.id.clearButton);
        returnButton = findViewById(R.id.returnButton);

        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setButtonClear();
                String lastString = stringLinkedList.removeLast();
            }
        });

        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = editText.getText().toString();
                stringLinkedList.addLast("Last text");
            }
        });

        //попробуй через декью

    }

    private void setButtonClear(String s) {
        buttonClear.setText(editText.getText().toString());
    }
}
